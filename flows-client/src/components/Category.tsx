import { Paper } from '@material-ui/core';
import React, { useState } from 'react';
import { ICategory } from '../store/data.interface';
import Flow from './Flow';

interface IProps {
  category?: ICategory;
}

function Category(props: IProps) {
  const { category } = props;
  const [expandedFlow, setExpanded] = useState('');

  const onExpand = (code: string) => {
    if (expandedFlow === code) {
      setExpanded('');
    } else {
      setExpanded(code);
    }
  };

  return (
    <Paper>
      {category &&
        category.flows.map((f) => (
          <Flow
            flow={f}
            key={`${f.code}-${Math.random() * 10}`}
            expanded={expandedFlow}
            onExpand={onExpand}
            category={category.title}
          />
        ))}
    </Paper>
  );
}

export default Category;
