import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  IconButton,
  Snackbar,
} from '@material-ui/core';
import { Close, ExpandMore } from '@material-ui/icons';
import React, { useState } from 'react';
import { IFlow } from '../store/data.interface';
import { Dispatch } from 'redux';
import { AppDispatch } from '../store';
import { removeFlow } from '../store/app.actions';
import { connect } from 'react-redux';

interface IProps {
  flow: IFlow;
  expanded: string;
  category: string;
  onExpand: (title: string) => void;
  removeFlow: (code: string, category: string) => void;
}

function Flow(props: IProps) {
  const {
    flow: { code, name },
    expanded,
    removeFlow,
    category,
    onExpand,
  } = props;

  const [open, openSnackbar] = useState(false);
  const [currentAction, setAction] = useState<string>('');

  const handleClose = () => {
    openSnackbar(false);
  };

  const handleAction = (action: string) => {
    setAction(action);
    openSnackbar(true);
  };

  return (
    <Accordion
      expanded={expanded === code}
      onChange={onExpand.bind(null, code)}
    >
      <AccordionSummary
        expandIcon={<ExpandMore />}
        aria-controls={`flow-${code}-content`}
        id={`flow-${code}-header`}
      >
        {name || code}
      </AccordionSummary>
      <AccordionDetails>
        <Button onClick={handleAction.bind(null, 'SCE')}>SCE</Button>
        <Button onClick={handleAction.bind(null, 'API')}>API</Button>
        <Button onClick={handleAction.bind(null, 'POSTMAN')}>
          Open postman
        </Button>
        <Button onClick={handleAction.bind(null, 'COPY')}>Copy</Button>
        <Button onClick={removeFlow.bind(null, code, category)}>Delete</Button>
        <Button onClick={handleAction.bind(null, 'NEW')}>New</Button>

        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          open={open}
          message={`Action: ${currentAction} - code: ${code}`}
          action={
            <React.Fragment>
              <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={handleClose}
              >
                <Close fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
      </AccordionDetails>
    </Accordion>
  );
}

const mapDispatchToProps = (dispatch: Dispatch<AppDispatch>) => ({
  removeFlow: (code: string, category: string) =>
    dispatch(removeFlow({ code, category })),
});

export default connect(null, mapDispatchToProps)(Flow);
