export interface IData {
  categories: Array<ICategory>;
}

export interface ICategory {
  title: string;
  flows: Array<IFlow>;
}

export interface IFlow {
  code: string;
  name?: string;
}
