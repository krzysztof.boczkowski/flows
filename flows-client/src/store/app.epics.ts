import { Epic, ofType, combineEpics } from 'redux-observable';
import {
  getCategories,
  getCategoriesSuccess,
  addFlow,
  addFlowSuccess,
  removeFlow,
  removeFlowSuccess,
} from './app.actions';
import { api } from './api';
import { switchMap, pluck } from 'rxjs/operators';
import { of, combineLatest } from 'rxjs';
import { IAddFlowPayload, IRemoveFlowPayload } from './app.actions.type';

const getCategories$: Epic = (action$) =>
  action$.pipe(
    ofType(getCategories.type),
    switchMap(() => api.getCategories()),
    switchMap((data) => of(getCategoriesSuccess(data.categories))),
  );

const addFlow$: Epic = (action$) =>
  action$.pipe(
    ofType(addFlow.type),
    pluck('payload'),
    switchMap((payload: IAddFlowPayload) =>
      combineLatest(api.addFlow(payload), of(payload)),
    ),
    switchMap(([_, payload]) => of(addFlowSuccess(payload))),
  );

const removeFlow$: Epic = (action$) =>
  action$.pipe(
    ofType(removeFlow.type),
    pluck('payload'),
    switchMap((payload: IRemoveFlowPayload) =>
      combineLatest(api.removeFlow(payload), of(payload)),
    ),
    switchMap(([_, payload]) => of(removeFlowSuccess(payload))),
  );

export const appEpics = combineEpics(getCategories$, addFlow$, removeFlow$);
