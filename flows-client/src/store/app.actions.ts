import { createAction } from '@reduxjs/toolkit';
import { IAddFlowPayload, IRemoveFlowPayload } from './app.actions.type';
import { ICategory } from './data.interface';

export const getCategories = createAction<undefined, 'GET_CATEGORIES'>(
  'GET_CATEGORIES',
);

export const getCategoriesSuccess = createAction<
  Array<ICategory>,
  'GET_CATEGORIES_SUCCESS'
>('GET_CATEGORIES_SUCCESS');

export const addFlow = createAction<IAddFlowPayload, 'ADD_FLOW'>('ADD_FLOW');

export const addFlowSuccess = createAction<IAddFlowPayload, 'ADD_FLOW_SUCCESS'>(
  'ADD_FLOW_SUCCESS',
);

export const removeFlow = createAction<IRemoveFlowPayload, 'REMOVE_FLOW'>(
  'REMOVE_FLOW',
);

export const removeFlowSuccess = createAction<
  IRemoveFlowPayload,
  'REMOVE_FLOW_SUCCESS'
>('REMOVE_FLOW_SUCCESS');
