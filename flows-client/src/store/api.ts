import { from } from 'rxjs';
import { IAddFlowPayload, IRemoveFlowPayload } from './app.actions.type';
import { IData } from './data.interface';
import axios from 'axios';

export const api = {
  getCategories: () => {
    const request: Promise<IData> = axios
      .get('http://localhost:4000/flows')
      .then((res) => res.data);
    return from(request);
  },
  addFlow: (body: IAddFlowPayload) => {
    const request: Promise<null> = axios
      .post('http://localhost:4000/flows', body)
      .then((res) => res.data);

    return from(request);
  },
  removeFlow: (body: IRemoveFlowPayload) => {
    const request: Promise<null> = axios
      .post(`http://localhost:4000/flows/remove`, body)
      .then((res) => res.data);

    return from(request);
  },
};
