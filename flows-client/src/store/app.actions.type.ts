import { IFlow } from './data.interface';

export interface IAddFlowPayload {
  category: string;
  flow: IFlow;
}

export interface IRemoveFlowPayload {
  category: string;
  code: string;
}
