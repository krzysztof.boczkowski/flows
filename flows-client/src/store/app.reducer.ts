import { createReducer } from '@reduxjs/toolkit';
import {
  getCategoriesSuccess,
  addFlowSuccess,
  removeFlowSuccess,
} from './app.actions';
import { IData } from './data.interface';

const initState: IData = {
  categories: [],
};

export const appReducer = createReducer(initState, (builder) =>
  builder
    .addCase(getCategoriesSuccess, (state, action) => {
      state.categories = action.payload;
    })
    .addCase(addFlowSuccess, (state, action) => {
      const category = state.categories.find(
        (c) => c.title === action.payload.category,
      );
      category?.flows.push(action.payload.flow);
    })
    .addCase(removeFlowSuccess, (state, action) => {
      const category = state.categories.find(
        (c) => c.title === action.payload.category,
      );
      if (category) {
        category.flows = category.flows.filter(
          (f) => f.code !== action.payload.code,
        );
      }
    }),
);
