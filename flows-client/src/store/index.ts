import { configureStore } from '@reduxjs/toolkit';
import { appReducer } from './app.reducer';
import { createEpicMiddleware } from 'redux-observable';
import { appEpics } from './app.epics';

const epicMiddleware = createEpicMiddleware();

export const store = configureStore({
  reducer: appReducer,
  middleware: [epicMiddleware],
  devTools: process.env.NODE_ENV !== 'production',
});
epicMiddleware.run(appEpics);

export type AppState = ReturnType<typeof store.getState>;
export type AppDispatch = ReturnType<typeof store.dispatch>;

if (process.env.NODE_ENV !== 'production' && (module as any).hot) {
  (module as any).hot.accept('./', () => store.replaceReducer(appReducer));
}
