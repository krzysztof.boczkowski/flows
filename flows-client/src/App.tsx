import {
  Fade,
  Grid,
  MenuItem,
  MenuList,
  Paper,
  withStyles,
} from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import './App.css';
import Category from './components/Category';
import { AppDispatch, AppState } from './store';
import { getCategories } from './store/app.actions';
import { ICategory } from './store/data.interface';

const CustomMenuItem = withStyles((theme) => ({
  root: {
    '&.active': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
  },
}))(MenuItem);

interface IProps {
  categories: Array<ICategory>;
  getCategories: () => void;
}

interface IState {
  selected: ICategory | null;
}

class App extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      selected: null,
    };
  }

  componentDidMount() {
    this.props.getCategories();
  }

  selectMenuItem(selected: ICategory) {
    this.setState({ selected });
  }

  getSelectedCategory() {
    const { categories } = this.props;
    const { selected } = this.state;

    if (selected) {
      return categories.find((c) => c.title === selected.title);
    }
  }

  render() {
    const { categories } = this.props;
    const { selected } = this.state;

    return (
      <div className="app">
        <h1>Flows</h1>
        <Fade in={categories && !!categories.length}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={4}>
              <Paper elevation={1} className="side-menu">
                <MenuList>
                  {categories.map((c: ICategory) => (
                    <CustomMenuItem
                      onClick={this.selectMenuItem.bind(this, c)}
                      className={selected?.title === c.title ? 'active' : ''}
                      key={`menu-item-${c.title}`}
                    >
                      {c.title}
                    </CustomMenuItem>
                  ))}
                </MenuList>
              </Paper>
            </Grid>
            <Grid item xs={12} md={8}>
              {selected && categories && (
                <Category category={this.getSelectedCategory()} />
              )}
            </Grid>
          </Grid>
        </Fade>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  categories: state.categories,
});

const mapDispatchToProps = (dispatch: Dispatch<AppDispatch>) => ({
  getCategories: () => dispatch(getCategories()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
