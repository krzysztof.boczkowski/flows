# Flows

## Client

Use `npm install` and `npm run start` to launch React frontend app at `http://localhost;3000`

## Server

Use `npm install` and `npm run start` to launch backend written in `Nest.js`. It will be available at `http://localhost:4000`
