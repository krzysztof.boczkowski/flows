import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { IData } from './mocks/data.interface';
import { AddFlowDto } from './model/AddFlowDto';
import { RemoveFlowDto } from './model/RemoveFlowDto';

@Controller('flows')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getMenuData(): IData {
    return this.appService.getCategoriesData();
  }

  @Post()
  addFlow(@Body() addFlowDto: AddFlowDto) {
    return this.appService.addFlow(addFlowDto);
  }

  @Post('remove')
  removeFlow(@Body() removeFlowDto: RemoveFlowDto) {
    return this.appService.removeFlow(removeFlowDto);
  }
}
