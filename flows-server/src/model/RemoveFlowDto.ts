import { IsNotEmpty } from 'class-validator';

export class RemoveFlowDto {
  @IsNotEmpty()
  category: string;

  @IsNotEmpty()
  code: string;
}
