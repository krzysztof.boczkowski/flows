import { IsNotEmpty } from 'class-validator';
import { IFlow } from '../mocks/data.interface';

export class Flow implements IFlow {
  @IsNotEmpty()
  code: string;

  name?: string;
}
