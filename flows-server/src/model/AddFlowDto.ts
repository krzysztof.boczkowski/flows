import { IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { Flow } from './Flow';

export class AddFlowDto {
  @IsNotEmpty()
  category: string;

  @IsNotEmpty()
  @ValidateNested()
  @Type(() => Flow)
  flow: Flow;
}
