import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ICategory, IData } from './mocks/data.interface';
import { AddFlowDto } from './model/AddFlowDto';
import { RemoveFlowDto } from './model/RemoveFlowDto';
import * as jsonData from './mocks/data.json';

@Injectable()
export class AppService {
  private categoriesData: IData;

  constructor() {
    this.categoriesData = jsonData;
  }

  private getCategory(category: string): ICategory {
    let currentCategory: ICategory = this.categoriesData.categories.find(
      (c) => c.title === category,
    );

    if (!currentCategory) {
      throw new HttpException(
        `Could not find category ${category}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    return currentCategory;
  }

  public getCategoriesData(): IData {
    return this.categoriesData;
  }

  public addFlow({ category, flow }: AddFlowDto) {
    const currentCategory = this.getCategory(category);
    if (currentCategory.flows.find((f) => f.code === flow.code)) {
      throw new HttpException(
        `Flow with code: ${flow.code} is not unique`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
    currentCategory.flows.push(flow);
  }

  public removeFlow({ category, code }: RemoveFlowDto) {
    const currentCategory: ICategory = this.getCategory(category);
    if (!currentCategory.flows.find((f) => f.code === code)) {
      throw new HttpException(
        `Could not find flow with code: ${code}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    currentCategory.flows = currentCategory.flows.filter(
      (f) => f.code !== code,
    );
  }
}
